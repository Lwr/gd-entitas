class_name TearDownProcessor
extends Node

func teardown():
	printerr("Subclass must implement this method")
