class_name InitializeProcessor
extends Node
"""
Usage:
	_ready():
		all_of(['Position'])
	
	initialize_entities(entities_with_position): ...
"""

var _group : Group
var _dict_matcher = {}
var context

func set_context(ctx: Context):
	context = ctx
	_group = ctx.get_group(with_entities())


func with_entities() -> Matcher:
	return Matcher.new(_dict_matcher)


func all_of(component_names: Array):
	_dict_matcher['all_of'] = component_names


func any_of(component_names: Array):
	_dict_matcher['any_of'] = component_names


func none_of(component_names: Array):
	_dict_matcher['any_of'] = component_names


func initialize():
	initialize_entities(_group.get_entities().values())


func initialize_entities(entities):
	printerr("Subclass must implement this method")
