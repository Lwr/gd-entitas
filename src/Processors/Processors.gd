class_name Processors
extends Node

var context

var initialize_processors : Array[InitializeProcessor] = []
var execute_processors : Array[ExecuteProcessor] = []
var cleanup_processors : Array[CleanupProcessor] = []
var teardown_processors : Array[TearDownProcessor] = []


func _ready():
	context = get_parent()
	for child in get_children():
		add(child)


func overriden_add_child(node: Node, legible_unique_name: bool = false, internal: InternalMode = 0):
	super.add_child(node, legible_unique_name, internal)
	add(node)


func add(child) -> void:
	if child is InitializeProcessor:
		initialize_processors.append(child)
	elif child is ExecuteProcessor:
		execute_processors.append(child)
	elif child is CleanupProcessor:
		cleanup_processors.append(child)
	elif child is TearDownProcessor:
		teardown_processors.append(child)
	elif child is Node:  # Recursive case for organization purposes
		for c in child.get_children():
			add(c)


func initialize() -> void:
	for processor in initialize_processors:
		processor.initialize()


func execute(delta) -> void:
	for processor in execute_processors:
		if processor.active:
			processor.execute(delta)


func execute_physics(delta) -> void:
	for processor in execute_processors:
		if processor.active:
			processor.execute_physics(delta)


func cleanup() -> void:
	for processor in cleanup_processors:
		processor.cleanup()


func teardown() -> void:
	for processor in teardown_processors:
		processor.teardown()


func activate_reactive_processors() -> void:
	for processor in execute_processors:
		if processor is ReactiveProcessor:
			processor.activate()
		
#		if processor is Processors:
#			processor.activate_reactive_processors()


func deactivate_reactive_processors() -> void:
	for processor in execute_processors:
		if processor is ReactiveProcessor:
			processor.deactivate()
		
#		if processor is Processors:
#			processor.deactivate_reactive_processors()


func clear_reactive_processors() -> void:
	for processor in execute_processors:
		if processor is ReactiveProcessor:
			processor.clear()
		
#		if processor is Processors:
#			processor.clear_reactive_processors()
