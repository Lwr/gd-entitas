class_name ForeachProcessor
extends ExecuteProcessor
"""
Usage:
	_ready():
		all_of(['Position'])
	
	_foreach(entities_with_position, delta): ...
"""

var _group : Group
var _dict_matcher := {}
var context


func set_context(ctx: Context):
	context = ctx
	_group = ctx.get_group(with_entities())


func with_entities() -> Matcher:
	return Matcher.new(_dict_matcher)


func all_of(component_names: Array[GDScript]):
	_dict_matcher['all_of'] = component_names


func any_of(component_names: Array[GDScript]):
	_dict_matcher['any_of'] = component_names


func none_of(component_names: Array[GDScript]):
	_dict_matcher['any_of'] = component_names


func get_foreach_entities() -> Array[Entity]:
	return _group.get_entities().values()


func execute(delta):
	foreach(_group.get_entities().values(), delta)


func execute_physics(delta):
	foreach_physics(_group.get_entities().values(), delta)


func foreach(entities: Array, delta: float):
	# printerr("Subclass must implement this method")
	pass


func foreach_physics(entities: Array, delta: float):
	# printerr("Subclass must implement this method")
	pass
