class_name IntervalProcessor
extends ExecuteProcessor
"""
Execute processor that executes every interval
"""

# Interval in seconds
@export var interval: float = 1  # 1 each second default

var time_without_executing := 0
var time_without_executing_phys := 0


func execute(delta: float) -> void:
	time_without_executing += delta
	if time_without_executing >= interval:
		execute_interval()
		time_without_executing = 0


func execute_physics(delta: float) -> void:
	time_without_executing_phys += delta
	if time_without_executing_phys >= interval:
		execute_physics_interval()
		time_without_executing_phys = 0


func execute_interval() -> void:
	# Should be overriden
	pass


func execute_physics_interval() -> void:
	# Should be overriden
	pass
