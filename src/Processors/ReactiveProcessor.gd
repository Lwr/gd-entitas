class_name ReactiveProcessor
extends ExecuteProcessor

var _collector: Collector
var _buffer := []

func set_context(ctx: Context):
	_collector = _get_collector(ctx)


func get_trigger():
	printerr("Subclass must implement this method")


func filter(entity: Entity):
	return true


func react(entities: Array, _delta: float):
	printerr("Subclass must implement this method")


func activate():
	_collector.activate()


func deactivate():
	_collector.deactivate()


func clear():
	_collector.clear_collected_entities()


func execute(delta):
	for entity in _collector._collected_entities.values():
		if filter(entity):
			_buffer.append(entity)
	
	_collector.clear_collected_entities()
	
	if not _buffer.is_empty():
		react(_buffer, delta)
		_buffer.clear()


func _get_collector(ctx: Context) -> Collector:
	var trigger = get_trigger()
	var collector = Collector.new()
	
	for match_and_group_event in trigger:
		var matcher = match_and_group_event[0]
		var group_event = match_and_group_event[1]
		
		var group = ctx.get_group(matcher)
		collector.add(group, group_event)
	
	return collector
