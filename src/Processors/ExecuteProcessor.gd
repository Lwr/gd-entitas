class_name ExecuteProcessor
extends Node

@export var active : bool = true : set = set_active

func execute(_delta):
	# printerr("Subclass must implement this method")
	pass

func execute_physics(_delta):
	# printerr("Subclass must implement this method")
	pass


func set_active(value):
	active = value
	set_process(value)
