class_name Matcher
extends Object

var _all: Array
var _any: Array
var _none: Array



func _init(kwargs: Dictionary) -> void:
	"""
	:param kwargs: Dictionary with
	* `all_of` list of component_types (String)
	* `any_of` list of component_types (String)
	* `none_of` list of component_types (String)
	"""
	_all = kwargs.get('all_of', [])
	_any = kwargs.get('any_of', [])
	_none = kwargs.get('none_of', [])
	if _all: _all.sort()
	if _any: _all.sort()
	if _none: _all.sort()


func matches(entity: Entity) -> bool:
	var all_cond = _all.is_empty() or entity.has(_all)
	var any_cond = _any.is_empty() or entity.has_any(_any)
	var none_cond = _none.is_empty() or not entity.has_any(_none)
	return all_cond and any_cond and none_cond


func _to_string() -> String:
	return '<Matcher [all=(%s) any=(%s) none=(%s)]>' % [
		get_expr_repr(_all),
		get_expr_repr(_any),
		get_expr_repr(_none),
	]


func get_key() -> String:
	"""
	Returns a unique key based on the keys, sorted by lexicographical order.
	First all, then any, and then none
	"""
	return _to_string()


func get_expr_repr(expr: Array) -> String:
	return '' if expr == null else ', '.join(expr)
