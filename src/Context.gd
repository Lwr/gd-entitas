class_name Context
extends Node
# A context is a data structure responsible of managing entities.

#: Entities retained by this context.
var _entities := {} : get = get_entities
func get_entities():
	return _entities

#: An object pool to recycle entities.
var _reusable_entities : Array[Entity] = []

#: Entities counter.
var _entity_index: int = 0

#: Dictionary of matchers mapping groups.
var _groups := {}

var _entity_indices := {}


func has_entity(entity: Entity) -> bool:
	"""Checks if the context contains this entity."""
	return _entities.has(entity.get_instance_id())


func create_entity(entity_class) -> Entity:
	"""
	Creates an entity. Pop one entity from the pool if it is not
	empty, otherwise creates a new one. Increments the entity index.
	"""
	var entity: Entity = (_reusable_entities.pop_back() if not _reusable_entities.is_empty()
														else ClassDB.instantiate(entity_class).new())
	entity.activate(_entity_index)
	_entity_index += 1
	_entities[entity.get_instance_id()] = entity
	entity.connect("on_component_added",Callable(self,"_comp_added_or_removed"))
	entity.connect("on_component_removed",Callable(self,"_comp_added_or_removed"))
	entity.connect("on_component_replaced",Callable(self,"_comp_replaced"))
	
	return entity


func add_entity(entity: Entity) -> Entity:
	"""Adds an entity. Increments the entity index"""
	entity.activate(_entity_index)
	_entity_index += 1
	_entities[entity.get_instance_id()] = entity
	
	entity.connect("on_component_added",Callable(self,"_comp_added_or_removed"))
	entity.connect("on_component_removed",Callable(self,"_comp_added_or_removed"))
	entity.connect("on_component_replaced",Callable(self,"_comp_replaced"))
	
	return entity


func destroy_entity(entity: Entity):
	"""Removes an entity from the list and add it to the pool"""
	assert(has_entity(entity))
	entity._destroy()
	_entities.erase(entity.get_instance_id())
	# entity.queue_free()
	# Probably queue_free?
	# see how to use _reusable_entities


func get_group(matcher: Matcher) -> Group:
	"""User can ask for a group of entities from the context. The group is identified through a
	:class:`Matcher`"""
	var matcher_id = matcher.get_key()
	if _groups.has(matcher_id):
		return _get_group_to_return(matcher_id, matcher)
	
	var group = Group.new(matcher)
	for entity in _entities.values():
		group.handle_entity_silently(entity)
	
	_groups[matcher_id] = group
	return group


func _get_group_to_return(matcher_id : String, matcher : Matcher):
	var group_to_return = _groups[matcher_id]
	if group_to_return._matcher.get_instance_id() != matcher.get_instance_id():
		matcher.free()
	return group_to_return


func _comp_added_or_removed(entity: Entity, comp=null):
	for group in _groups.values():
		group.handle_entity(entity, comp)


func _comp_replaced(entity: Entity, previous_comp: Component, new_comp: Component):
	for group in _groups.values():
		group.update_entity(entity, previous_comp, new_comp)


func _to_string() -> String:
	return '<Context (%s/%s)>' % [len(_entities), len(_groups)]
