class_name Group
extends Object
# Use context.get_group(matcher) to get a group of entities which
# match the specified matcher. Calling context.get_group(matcher) with
# the same matcher will always return the same instance of the group.
#
# The created group is managed by the context and will always be up to
# date. It will automatically add entities that match the matcher or
# remove entities as soon as they don't match the matcher anymore.

#: Occurs when an entity gets added.
signal on_entity_added(entity, component)

#: Occurs when an entity gets removed.
signal on_entity_removed(entity, component)

#: Occurs when a component of an entity in the group gets replaced.
signal on_entity_updated(entity, previous, new)


enum GroupEvent {ADDED, REMOVED, ADDED_OR_REMOVED}


var _matcher: Matcher
var _entities := {} : get = get_entities
func get_entities():
	return _entities


func _init(matcher: Matcher) -> void:
	_matcher = matcher


func single_entity():
	"""Returns the only entity in this group."""
	var count = len(_entities)
	assert(count <= 1)
	return _entities.values().front() if count == 1 else null


func handle_entity_silently(entity: Entity) -> void:
	"""This is used by the context to manage the group."""
	if _matcher.matches(entity):
		_add_entity_silently(entity)
	else:
		_remove_entity_silently(entity)

func handle_entity(entity: Entity, component: Component) ->  void:
	"""This is used by the context to manage the group."""
	# print("Handling entity %s from group %s" % [entity, self])
	if _matcher.matches(entity):
		_add_entity(entity, component)
	else:
		_remove_entity(entity, component)


func update_entity(entity: Entity, previous_comp: Component, new_comp: Component) -> void:
	"""This is used by the context to manage the group."""
	if _entities.has(entity.get_instance_id()):
		emit_signal("on_entity_removed", entity, previous_comp)
		emit_signal("on_entity_added", entity, new_comp)
		emit_signal("on_entity_updated", entity, previous_comp, new_comp)


func _add_entity_silently(entity: Entity) -> bool:
	var entity_id = entity.get_instance_id()
	if not _entities.has(entity_id):
		_entities[entity_id] = entity
		return true
	return false


func _add_entity(entity: Entity, component: Component) -> void:
	if _add_entity_silently(entity):
		emit_signal("on_entity_added", entity, component)


func _remove_entity_silently(entity: Entity) -> bool:
	var entity_id = entity.get_instance_id()
	if _entities.has(entity_id):
		_entities.erase(entity_id)
		return true
	return false


func _remove_entity(entity: Entity, component: Component) -> void:
	if _remove_entity_silently((entity)):
		emit_signal("on_entity_removed", entity, component)


func _to_string() -> String:
	return '<Group [%s]>' % [_matcher]
