# entitas.entity
# ~~~~~~~~~~~~~~
# An entity is a container holding data to represent certain
# objects in your application. You can add, replace or remove data
# from entities.
# Those containers are called 'components'. They are represented by
# namedtuples for readability.


class_name Entity
extends Node3D
# Use context.create_entity() to create a new entity and
# context.destroy_entity() to destroy it.
# You can add, replace and remove components to an entity.


#: Occurs when a component gets added.
signal on_component_added(entity, component)

#: Occurs when a component gets removed.
signal on_component_removed(entity, component)

#: Occurs when a component gets replaced.
signal on_component_replaced(entity, previous, component)


#: Dictionary mapping component type and component instance.
var _components := {}

#: Each entity has its own unique creationIndex which will be
#: set by the context when you create the entity.
var _creation_index: int = 0

#: The context manages the state of an entity.
#: Active entities are enabled, destroyed entities are not.
@export var _is_enabled: bool = false

func _ready():
	print("for debug purposes")


func activate(creation_index) -> void:
	_creation_index = creation_index
	_is_enabled = true


func add(component: Component) -> void:
	"""Adds a component"""
	assert(_is_enabled)
	assert(not has_single(component.get_script()))
	_components[component.get_script()] = component
	_add_component_child(component)
	emit_signal("on_component_added", self, component)


func remove(comp_type: GDScript) -> void:
	"""Removes a component"""
	assert(_is_enabled)
	assert(has([comp_type]))
	_replace(comp_type, null)


func replace(comp_type: GDScript, component: Component) -> void:
	"""Replaces an existing component or adds it if it doesn't exist yet."""
	assert(_is_enabled)
	
	if has([comp_type]):
		_replace(comp_type, component)
	else:
		add(component)


func get_comp(comp_type: GDScript):
	assert(has([comp_type]))
	return _components[comp_type]


func has(component_types: Array) -> bool:
	"""Checks if the entity has all components of the given type(s)"""
	return _components.has_all(component_types)


func has_any(component_types: Array) -> bool:
	"""Checks if the entity has any components of the given type(s)"""
	return component_types.any(has_single)


func remove_all() -> void:
	"""Removes all components"""
	for comp_key in _components.keys():
		_replace(comp_key, null)


### Private functions ###

func _replace(comp_type: GDScript, component) -> void:
	var components = get_node("Components")
	if component != null:
		var previous = _components[comp_type]
		components.remove_child(previous)
		previous.queue_free()
		_components[comp_type] = component
		components.add_child(component)
		emit_signal("on_component_replaced", self, previous, component)
	else:
		var previous = _components[comp_type]
		components.remove_child(previous)
		previous.queue_free()
		_components.erase(comp_type)
		emit_signal("on_component_removed", self, previous)


func _destroy() -> void:
	"""
	This method is used internally. Don't call it yourself.
	Use context.destroy_entity(entity) instead.
	"""
	_is_enabled = false
	remove_all()


func _add_component_child(component : Component) -> void:
	var components = get_node('Components')
	var the_name :StringName = component.get_class_name()
	if not components.has_node(component.get_class_name()):
		components.add_child(component)


func _to_string() -> String:
	""" <Entity_0 [Position(x=1, y=2, z=3)]> """
	return '<Entity_%s [%s]>' % [
		_creation_index, ", ".join(_components.values().map(convert_to_string))
	]


func has_single(comp_type: GDScript) -> bool:
	return has([comp_type])


func convert_to_string(o) -> String:
	return o._to_string()
