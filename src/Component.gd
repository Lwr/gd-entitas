class_name Component
extends Node3D
# A set of variables. The idea is that Components should be as simple as possible.

# func is_class(p_name): return p_name == get_class() or super.is_class(p_name)


func get_entity() -> Entity:
	"""The entity holding the component is expected to be two levels above"""
	return get_parent().get_parent()


func get_component(comp_name: GDScript):
	var entity := get_entity()
	if entity.has_single(comp_name):
		return entity.get_comp(comp_name)


func _to_string() -> String:
	""" <Component Position(x=1, y=2, z=3)> """
	return '<Component %s>' % [get_class_name()]

func get_class_name():
	printerr("Must be overriden")
