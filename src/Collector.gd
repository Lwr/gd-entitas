class_name Collector
extends Object

var _collected_entities := {} : get = get_collected_entities
func get_collected_entities():
	return _collected_entities


var _groups = {}


func add(group: Group, group_event: Group.GroupEvent) -> void:
	_groups[group.get_instance_id()] = group_event


func activate() -> void:
	for group_id in _groups.keys():
		var group: Group = instance_from_id(group_id)
		var group_event = _groups[group_id]
		
		var added_event = group_event == Group.GroupEvent.ADDED
		var removed_event = group_event == Group.GroupEvent.REMOVED
		var added_or_removed_event = group_event == Group.GroupEvent.ADDED_OR_REMOVED
		
		if added_event or added_or_removed_event:
			group.connect("on_entity_added",Callable(self,"_add_entity"))
			for entity in group.get_entities().values():
				_add_entity(entity)
		
		if removed_event or added_or_removed_event:
			group.connect("on_entity_removed",Callable(self,"_add_entity"))


func deactivate() -> void:
	for group_id in _groups.keys():
		var group = instance_from_id(group_id)
		group.disconnect("on_entity_added",Callable(self,"_add_entity"))
		group.disconnect("on_entity_removed",Callable(self,"_add_entity"))
	
	clear_collected_entities()


func clear_collected_entities() -> void:
	_collected_entities.clear()


func _add_entity(entity: Entity, _component=null) -> void:
	_collected_entities[entity.get_instance_id()] = entity


func _to_string():
	return '<Collector [%s]' % [
		', '.join(_groups.values().map(convert_to_string))
	]


func convert_to_string(o):
	return o._to_string()
