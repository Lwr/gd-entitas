Entitas GDScript 2
--------------------

# Entity Component System simple implementation

This project is a simple and lightweight implementation of the [Entitas](https://github.com/sschmid/Entitas) Entity Component System to be used in [GodotEngine](https://godotengine.org/).

## Performance

The implementation has been carried out without performance in mind, thus the project is recommended only in cases where that characteristic is not the main concern.

# Usage examples

The main tree in a project using entitas would look like this:

![image](images/standard_setup.png)

An example of a basic ForeachProcessor would be like this:

![image](images/foreach_example_1.png)
![image](images/foreach_example_2.png)

An example of a Component would be like this:

![image](images/component_example.png)

Entities should be part of a group called "Entities" and have a child called Components, where all the components should belong to.

![image](images/entity_example.png)
